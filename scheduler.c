#include "data_structure.h"
#include "intra_slice.h"
#include <pthread.h>
int num_of_slice=10;

void inter_slice_scheduler(slice_info_t* sli)
{
	inter_slice_sched_t* inter_slice_sched;
	inter_slice_sched=(inter_slice_sched_t*)malloc(sizeof(inter_slice_sched));

	//output is inter_slice_sched,so we need to consider how to deliver this output result
	//maybe use global structure to store inter_slice_sched information.
}
void *intra_slice_scheduler(intra_slice_input slice_input)
{
	intra_slice_result_t* intra_slice_result;
	intra_slice_result=(intra_slice_result_t*) malloc(sizeof(intra_slice_result));
	//output is intra_slice_result,so we need to consider how to deliver this output result
	//maybe use global structure to store intra_slice_result information.
	void (*schedule_ulsch_p)(module_id_t module_idP, frame_t frameP, sub_frame_t subframe) = NULL;
    void (*schedule_ue_spec_p)(module_id_t module_idP, frame_t frameP, sub_frame_t subframe, int *mbsfn_flag) = NULL;
    if (eNB->scheduler_mode == SCHED_MODE_DEFAULT) {
    schedule_ulsch_p = schedule_ulsch;
    schedule_ue_spec_p = schedule_dlsch;
  } else if (eNB->scheduler_mode == SCHED_MODE_FAIR_RR) {
    memset(dlsch_ue_select, 0, sizeof(dlsch_ue_select));
    schedule_ulsch_p = schedule_ulsch_fairRR;
    schedule_ue_spec_p = schedule_ue_spec_fairRR;
  }
	schedule_SI_intra_slice(module_id_t module_idP, frame_t frameP, sub_frame_t subframeP);
}
void final_scheduler(intra_slice_result_t* schedule_res,nfapi_dl_config_request_body_t* dl_config_request_body)
{ 
	nfapi_dl_config_request_body_t* DL_config_body =dl_config_request_body;//already malloc()?
	nfapi_dl_config_request_pdu_t* DL_config_pdu;
	DL_config_pdu=(nfapi_dl_config_request_pdu_t*)malloc(sizeof(nfapi_dl_config_request_pdu_t));
}
void main()
{
	inter_slice_scheduler(slice_info_t* sli);
	pthread_t slice_threads[num_of_slice];

	for(int Num_threadCreate=0;Num_threadCreate<num_of_slice;Num_threadCreate++)
	{	
		//create all slice thread at here.
		pthread_create(&slice_threads[Num_threadCreate], NULL, intra_slice_scheduler, (void *)/*inputData*/);
	}
	//join all slice thread ,get the schedule result by each slice_thread.
	pthread_join();
	final_scheduler();
}