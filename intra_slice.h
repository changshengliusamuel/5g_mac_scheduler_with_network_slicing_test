


void schedule_SI_in_intra_slice(module_id_t module_idP, frame_t frameP, sub_frame_t subframeP);
void schedule_PCH_in_intra_slice(module_id_t module_idP,frame_t frameP,sub_frame_t subframeP);
void schedule_RA_in_intra_slice(module_id_t module_idP, frame_t frameP, sub_frame_t subframeP);
void schedule_ulsch_rnti(module_id_t module_idP,int slice_idx,frame_t frameP,sub_frame_t subframeP,unsigned char sched_subframeP,uint16_t      *first_rb)