#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* brief maximum number of slices*/
#define MAX_NUM_SLICES 10
#define NFAPI_CC_MAX 4    //Nfapi_interface.h
/* brief maximum number of resource block groups */
#define N_RBG_MAX 25    // for 20MHz channel BW
#define MAX_MOBILES_PER_ENB 256 //Platform_constants.h

/*schedule_algorithum*/
typedef void (*schedule_algorithum)(module_id_t         mod_id,
                                    int                 slice_id,
                                    frame_t             frame,
                                    sub_frame_t         subframe,
                                    intra_slice_input_t *intra_slice_input[slice_id],
                                    int                 max_nb_rb[slice_id][NFAPI_CC_MAX],
                                    rballoc_slice_t     *rballoc_slice[slice_id],
                                    int                 maxCCE[slice_id][NFAPI_CC_MAX]);

/* slice specific scheduler for the DL */
typedef void (*slice_scheduler_dl)(module_id_t mod_id,
                                   int         slice_idx,
                                   frame_t     frame,
                                   sub_frame_t subframe,
                                   int        *mbsfn_flag);

/* Configuration structure according to the configuration files*/
typedef struct {
  int slice_id;
  int pos_low[NFAPI_CC_MAX];
  int pos_high[NFAPI_CC_MAX];
  int pct[NFAPI_CC_MAX];
  /// name of available scheduler
  char     *sched_name;
  /// pointer to the slice specific scheduler in DL
  slice_scheduler_dl sched_cb;
}slice_input_t;


// brief Primitive exchanged between RLC and MAC informing about the buffer occupancy of the RLC protocol instance.
typedef  struct {
  // brief Bytes buffered in RLC protocol instance.
  rlc_buffer_occupancy_t       bytes_in_buffer; 
  // brief Number of PDUs buffered in RLC protocol instance (OBSOLETE)
  rlc_buffer_occupancy_t       pdus_in_buffer;  
  // brief Head SDU creation time.
  frame_t                      head_sdu_creation_time;          
  // brief remaining size of sdu: could be the total size or the remaining size of already segmented sdu
  sdu_size_t                   head_sdu_remaining_size_to_send;  
  // brief 0 if head SDU has not been segmented, 1 if already segmented 
  boolean_t                    head_sdu_is_segmented;
} mac_rlc_status_resp_t;

typedef struct{
  ///dl buffer info
  uint32_t dl_buffer_info[MAX_NUM_LCID];
  /// total downlink buffer info
  uint32_t dl_buffer_total;
  /// total downlink pdus
  uint32_t dl_pdus_total;
  /// downlink pdus for each LCID
  uint32_t dl_pdus_in_buffer[MAX_NUM_LCID];
  /// creation time of the downlink buffer head for each LCID
  uint32_t dl_buffer_head_sdu_creation_time[MAX_NUM_LCID];
  /// maximum creation time of the downlink buffer head across all LCID
  uint32_t dl_buffer_head_sdu_creation_time_max;
  /// a flag indicating that the downlink head SDU is segmented
  uint8_t dl_buffer_head_sdu_is_segmented[MAX_NUM_LCID];
  /// size of remaining size to send for the downlink head SDU
  uint32_t dl_buffer_head_sdu_remaining_size_to_send[MAX_NUM_LCID];
}UE_TEMPLATE;

/// Structure for saving the output of each inter-slice scheduler in this scheudler
/// It will determine the boundary of each slice.
typedef struct{
  /* Which slices will active (1->active)*/
  int active_slice[MAX_NUM_SLICES];
  /* The maximum number of RBs that can be scheduled in the slice */
  int max_nb_rb[MAX_NUM_SLICES][NFAPI_CC_MAX];
  /* Resource Block allocation for each slice*/
  rballoc_slice_t rballoc_slice[MAX_NUM_SLICES];
  /*DL max CCE*/
  int maxCCE[MAX_NUM_SLICES][NFAPI_CC_MAX];
  /*UL max CCE*/
  int maxCCE_UL[MAX_NUM_SLICES][NFAPI_CC_MAX];
}inter_slice_sched_t;

/* Resource Block allocation for each slice*/
typedef struct {
  /* Resource block's position*/  
  int slice_alloc_mask[NFAPI_CC_MAX][N_RBG_MAX];
} rballoc_slice_t;


typedef struct {
  /* Total buffer size */  
  int dl_buffer_total[MAX_NUM_SLICES];
} UE_information_t;


typedef struct{
  intra_config_t intra_config;
  UE_list_t UE_list;
  int cqi[NFAPI_CC_MAX];
}intra_slice_input_t;

/* brief UE list used by eNB to order UEs/CC for scheduling*/
/* 還沒補裡面有call到的structure */
typedef struct {
  // Indicate which slice of this UE_list belongs to (new parameter)
  int slice_id;
  /// Dedicated information for UEs
  struct PhysicalConfigDedicated  *physicalConfigDedicated[MAX_NUM_CCs][NUMBER_OF_UE_MAX];
  /// DLSCH pdu  /* 2 = TBindex */ 
  DLSCH_PDU DLSCH_pdu[MAX_NUM_CCs][2][NUMBER_OF_UE_MAX];
  /// DCI template and MAC connection parameters for UEs
  UE_TEMPLATE UE_template[MAX_NUM_CCs][NUMBER_OF_UE_MAX];
  /// DCI template and MAC connection for RA processes
  int pCC_id[NUMBER_OF_UE_MAX];
  /// sorted downlink component carrier for the scheduler
  int ordered_CCids[MAX_NUM_CCs][NUMBER_OF_UE_MAX];
  /// number of downlink active component carrier
  int numactiveCCs[NUMBER_OF_UE_MAX];
  /// sorted uplink component carrier for the scheduler
  int ordered_ULCCids[MAX_NUM_CCs][NUMBER_OF_UE_MAX];
  /// number of uplink active component carrier
  int numactiveULCCs[NUMBER_OF_UE_MAX];
  /// number of downlink active component carrier
  uint8_t dl_CC_bitmap[NUMBER_OF_UE_MAX];
  /// eNB to UE statistics
  eNB_UE_STATS eNB_UE_stats[MAX_NUM_CCs][NUMBER_OF_UE_MAX];
  /// scheduling control info
  UE_sched_ctrl UE_sched_ctrl[NUMBER_OF_UE_MAX];
  int next[NUMBER_OF_UE_MAX];
  int head;
  int next_ul[NUMBER_OF_UE_MAX];
  int head_ul;
  int num_UEs;
  boolean_t active[NUMBER_OF_UE_MAX];
} UE_list_t;



typedef struct {
  uint8_t pdu_type;
  uint8_t pdu_size;
  union {
    nfapi_ul_config_ulsch_pdu       ulsch_pdu;
    nfapi_ul_config_ulsch_cqi_ri_pdu    ulsch_cqi_ri_pdu;
    nfapi_ul_config_ulsch_harq_pdu      ulsch_harq_pdu;
    nfapi_ul_config_ulsch_cqi_harq_ri_pdu ulsch_cqi_harq_ri_pdu;
    nfapi_ul_config_uci_cqi_pdu       uci_cqi_pdu;
    nfapi_ul_config_uci_sr_pdu        uci_sr_pdu;
    nfapi_ul_config_uci_harq_pdu      uci_harq_pdu;
    nfapi_ul_config_uci_sr_harq_pdu     uci_sr_harq_pdu;
    nfapi_ul_config_uci_cqi_harq_pdu    uci_cqi_harq_pdu;
    nfapi_ul_config_uci_cqi_sr_pdu      uci_cqi_sr_pdu;
    nfapi_ul_config_uci_cqi_sr_harq_pdu   uci_cqi_sr_harq_pdu;
    nfapi_ul_config_srs_pdu         srs_pdu;
    nfapi_ul_config_harq_buffer_pdu     harq_buffer_pdu;
    nfapi_ul_config_ulsch_uci_csi_pdu   ulsch_uci_csi_pdu;
    nfapi_ul_config_ulsch_uci_harq_pdu    ulsch_uci_harq_pdu;
    nfapi_ul_config_ulsch_csi_uci_harq_pdu  ulsch_csi_uci_harq_pdu;
    nfapi_ul_config_nulsch_pdu        nulsch_pdu;
    nfapi_ul_config_nrach_pdu       nrach_pdu;
  };
} nfapi_ul_config_request_pdu_t;

// Configuration structure according to the configuration files for each slice
typedef struct{
  // MAX MCS for each slice
  int maxmcs;
}intra_config_t;

// Data structure, store scheduling result for each intra-slice scheduler.
typedef struct{
  // #RBs remaining unallocated for each UE after the intra-slice scheduler.
  int nb_rbs_remaining_slice[MAX_NUM_SLICES]; 
  UE_sched_result_t UE_sched_result[MAX_NUM_UE];
}intra_slice_result_t;


/*UE scheduling result for each intra-slice scheduler(preprocessor)*/
typedef struct {
  int UE_id;
  // The positions of each UE in the frequency domain
  int RBG_allocation;
  // The remaining RBs that have not been assigned
  int nb_rbs_remaining[NFAPI_CC_MAX];
} UE_sched_result_t;


typedef struct {
  uint8_t pdu_type;
  uint8_t pdu_size;
  union {
    nfapi_dl_config_dci_dl_pdu  dci_dl_pdu;
    nfapi_dl_config_bch_pdu   bch_pdu;
    nfapi_dl_config_mch_pdu   mch_pdu;
    nfapi_dl_config_dlsch_pdu dlsch_pdu;
    nfapi_dl_config_pch_pdu   pch_pdu;
    nfapi_dl_config_prs_pdu   prs_pdu;
    nfapi_dl_config_csi_rs_pdu  csi_rs_pdu;
    nfapi_dl_config_epdcch_pdu  epdcch_pdu;
    nfapi_dl_config_mpdcch_pdu  mpdcch_pdu;
    nfapi_dl_config_nbch_pdu  nbch_pdu;
    nfapi_dl_config_npdcch_pdu  npdcch_pdu;
    nfapi_dl_config_ndlsch_pdu  ndlsch_pdu;
  };
} nfapi_dl_config_request_pdu_t;

/*********************原本OAI裡目前用在slice的data structure**************************/

/* slice specific scheduler for the DL */
typedef void (*slice_scheduler_dl)(module_id_t mod_id,int slice_idx, frame_t frame, sub_frame_t subframe,int *mbsfn_flag);

typedef struct {
    slice_id_t id;
    /// RB share for each slice
    float     pct;
    /// whether this slice is isolated from the others
    int       isol;
    int       prio;
    /// Frequency ranges for slice positioning
    int       pos_low;
    int       pos_high;
    // max mcs for each slice
    int       maxmcs;
    /// criteria for sorting policies of the slices
    uint32_t  sorting;
    /// Accounting policy (just greedy(1) or fair(0) setting for now)
    int       accounting;
    /// name of available scheduler
    char     *sched_name;
    /// pointer to the slice specific scheduler in DL
    slice_scheduler_dl sched_cb;
} slice_sched_conf_dl_t;

/* slice specific scheduler for the UL */
typedef void (*slice_scheduler_ul)(module_id_t mod_id,int slice_idx, frame_t frame, sub_frame_t subframe,unsigned char sched_subframe, uint16_t *first_rb);

typedef struct {
    slice_id_t id;
    /// RB share for each slice
    float     pct;
    // MAX MCS for each slice
    int       maxmcs;
    /// criteria for sorting policies of the slices
    uint32_t  sorting;
    /// starting RB (RB offset) of UL scheduling
    int       first_rb;
    /// name of available scheduler
    char     *sched_name;
    /// pointer to the slice specific scheduler in UL
    slice_scheduler_ul sched_cb;
} slice_sched_conf_ul_t;

typedef struct {
    /// indicates whether remaining RBs after first intra-slice allocation will be allocated to UEs of the same slice
    int       intraslice_share_active;
    /// indicates whether remaining RBs after slice allocation will be allocated to UEs of another slice. Isolated slices will be ignored
    int       interslice_share_active;

    /// number of active DL slices
    int      n_dl;
    slice_sched_conf_dl_t dl[MAX_NUM_SLICES];

    /// number of active UL slices
    int      n_ul;
    slice_sched_conf_ul_t ul[MAX_NUM_SLICES];
} slice_info_t;


typedef struct{
  /// indicator that CQI was received on PUSCH when requested eNB_dlsch_ulsch_sched()
  int32_t cqi_received;
  int32_t ul_inactivity_timer;
  // end
  int32_t ul_failure_timer; //check_ul_failure()
  int32_t ul_out_of_sync;   //check_ul_failure()
  int32_t ra_pdcch_order_sent; //check_ul_failure(), if request RA procedure set 1
  
  uint32_t ue_reestablishment_reject_timer;
  uint32_t ue_reestablishment_reject_timer_thres;  



}UE_sched_ctrl;



typedef struct {
  uint8_t pdu_type;
  uint8_t pdu_size;
  union {
    nfapi_dl_config_dci_dl_pdu  dci_dl_pdu;
    nfapi_dl_config_bch_pdu   bch_pdu;
    nfapi_dl_config_mch_pdu   mch_pdu;
    nfapi_dl_config_dlsch_pdu dlsch_pdu;
    nfapi_dl_config_pch_pdu   pch_pdu;
    nfapi_dl_config_prs_pdu   prs_pdu;
    nfapi_dl_config_csi_rs_pdu  csi_rs_pdu;
    nfapi_dl_config_epdcch_pdu  epdcch_pdu;
    nfapi_dl_config_mpdcch_pdu  mpdcch_pdu;
    nfapi_dl_config_nbch_pdu  nbch_pdu;
    nfapi_dl_config_npdcch_pdu  npdcch_pdu;
    nfapi_dl_config_ndlsch_pdu  ndlsch_pdu;
  };
} nfapi_dl_config_request_pdu_t;
// P7 Sub Structures
typedef struct {
  //check_ul_failure() start
  nfapi_tl_t tl;
  uint8_t dci_format;
  uint8_t aggregation_level;
  uint16_t rnti;
  uint32_t resource_block_coding;
  uint8_t rnti_type;
  uint16_t transmission_power;
  //check_ul_failure() end
  uint8_t mcs_1;
  uint8_t redundancy_version_1;
  uint8_t new_data_indicator_1;
  uint8_t harq_process;
  uint8_t tpc;
  uint8_t ngap;
  uint8_t virtual_resource_block_assignment_flag;

} nfapi_dl_config_dci_dl_pdu_rel8_t;

typedef struct {
  nfapi_tl_t tl;
  uint16_t length;
  int16_t pdu_index;
  uint16_t transmission_power;
} nfapi_dl_config_bch_pdu_rel8_t;

typedef struct { 
  nfapi_tl_t tl;
  uint32_t handle;
  uint16_t size;
  uint16_t rnti;
  uint8_t srs_bandwidth;
  uint8_t frequency_domain_position;
  uint8_t srs_hopping_bandwidth;
  uint8_t transmission_comb;
  uint16_t i_srs;
  uint8_t sounding_reference_cyclic_shift;
} nfapi_ul_config_srs_pdu_rel8_t;