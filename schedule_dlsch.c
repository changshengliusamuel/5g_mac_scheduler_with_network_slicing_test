#include "data_structure.h"
#include "samuel_pseudo.c"

void
schedule_dlsch(module_id_t module_idP, frame_t frameP, sub_frame_t subframeP, int *mbsfn_flag) {
  int i = 0;
  slice_info_t *sli = &RC.mac[module_idP]->slice_info;
  memset(sli->rballoc_sub, 0, sizeof(sli->rballoc_sub));
  
  inter_slice(module_idP, frameP, subframeP, sli);

  for (i = 0; i < sli->n_dl; i++) {
    // Run each enabled slice-specific schedulers one by one
    sli->dl[i].sched_cb(module_idP,
                        i,
                        frameP,
                        subframeP,
                        mbsfn_flag/*, dl_info*/);
  }
}

void
schedule_ue_spec_intra(module_id_t module_idP,
                       int slice_idxP,
                       frame_t frameP,
                       sub_frame_t subframeP,
                       int *mbsfn_flag,
                       inter_slice_sched_t inter_slice_sched)
//------------------------------------------------------------------------------
{
  COMMON_channels_t *cc = eNB->common_channels;
  eNB_STATS *eNB_stats = NULL;  
  int nb_mac_CC = RC.nb_mac_CC[module_idP];
  long dl_Bandwidth;
  int N_RB_DL[NFAPI_CC_MAX];
  int min_rb_unit[NFAPI_CC_MAX];
  int total_nb_available_rb[NFAPI_CC_MAX];
  int N_RBG[NFAPI_CC_MAX];
  eNB_MAC_INST *eNB = RC.mac[module_idP];
  UE_list_t *UE_list = &eNB->UE_list;  
  // ------------------------------------------

  // for TDD: check that we have to act here, otherwise return
  if (cc[0].tdd_Config) {
    tdd_sfa = cc[0].tdd_Config->subframeAssignment;

    switch (subframeP) {
      case 0:
        // always continue
        break;

      case 1:
      case 2:
        return;

      case 3:
        if (tdd_sfa != 2 && tdd_sfa != 5)
          return;

        break;

      case 4:
        if (tdd_sfa != 1 && tdd_sfa != 2 && tdd_sfa != 4 && tdd_sfa != 5)
          return;

        break;

      case 5:
        break;

      case 6:
      case 7:
        if (tdd_sfa != 3 && tdd_sfa != 4 && tdd_sfa != 5)
          return;

        break;

      case 8:
        if (tdd_sfa != 2 && tdd_sfa != 3 && tdd_sfa != 4 && tdd_sfa != 5)
          return;

        break;

      case 9:
        if (tdd_sfa == 0)
          return;

        break;
    }
  }

  for (CC_id = 0, eNB_stats = &eNB->eNB_stats[0]; CC_id < nb_mac_CC; CC_id++, eNB_stats++) {
    dl_Bandwidth = cc[CC_id].mib->message.dl_Bandwidth;
    N_RB_DL[CC_id] = to_prb(dl_Bandwidth);
    min_rb_unit[CC_id] = get_min_rb_unit(module_idP, CC_id);

    // get number of PRBs less those used by common channels
    // total_nb_available_rb[CC_id] = N_RB_DL[CC_id];
    // Modify by Gina
    total_nb_available_rb[CC_id] = inter_slice_sched->max_nb_rb[slice_idxP][CC_id];

    for (i = 0; i < N_RB_DL[CC_id]; i++)
      if (cc[CC_id].vrb_map[i] != 0)
        total_nb_available_rb[CC_id]--;

    N_RBG[CC_id] = to_rbg(dl_Bandwidth);
    // store the global enb stats:
    eNB_stats->num_dlactive_UEs = UE_list->num_UEs;
    eNB_stats->available_prbs = total_nb_available_rb[CC_id];
    eNB_stats->total_available_prbs += total_nb_available_rb[CC_id];
    eNB_stats->dlsch_bytes_tx = 0;
    eNB_stats->dlsch_pdus_tx = 0;
    // Get the final number of RBs that can be scheduled
    inter_slice_sched->max_nb_rb[slice_idxP][CC_id]=  eNB_stats->total_available_prbs;
  }

  // CALLING Pre_Processor for downlink scheduling
  // (Returns estimation of RBs required by each UE and the allocation on sub-band)
  dlsch_scheduler_pre_processor(module_idP,
                                slice_idxP,
                                frameP,
                                subframeP,
                                mbsfn_flag,
                                eNB->slice_info.rballoc_sub);



/*
  if (RC.mac[module_idP]->slice_info.interslice_share_active) 
  {
    dlsch_scheduler_interslice_multiplexing(module_idP,
                                            frameP,
                                            subframeP,
                                            eNB->slice_info.rballoc_sub);
    // the interslice multiplexing re-sorts the UE_list for the slices it tries
    //  to multiplex, so we need to sort it for the current slice again 
    sort_UEs(module_idP, slice_idxP, frameP, subframeP);
  }
*/
}

int to_prb(int dl_Bandwidth)
//------------------------------------------------------------------------------
{
  int prbmap[6] = { 6, 15, 25, 50, 75, 100 };
  AssertFatal(dl_Bandwidth < 6, "dl_Bandwidth is 0..5\n");
  return (prbmap[dl_Bandwidth]);
}

//------------------------------------------------------------------------------
int
get_min_rb_unit(module_id_t module_id,
                uint8_t CC_id)
//------------------------------------------------------------------------------
{
  int min_rb_unit = 0;
  int N_RB_DL = to_prb(RC.mac[module_id]->common_channels[CC_id].mib->message.dl_Bandwidth);

  switch (N_RB_DL) {
    case 6:     // 1.4 MHz
      min_rb_unit = 1;
      break;

    case 25:      // 5HMz
      min_rb_unit = 2;
      break;

    case 50:      // 10HMz
      min_rb_unit = 3;
      break;

    case 100:     // 20HMz
      min_rb_unit = 4;
      break;

    default:
      min_rb_unit = 2;
      LOG_W(MAC, "[eNB %d] N_DL_RB %d unknown for CC_id %d, setting min_rb_unit to 2\n",
            module_id,
            N_RB_DL,
            CC_id);
      break;
  }

  return min_rb_unit;
}

int
to_rbg(int dl_Bandwidth)
//------------------------------------------------------------------------------
{
  int rbgmap[6] = { 6, 8, 13, 17, 19, 25 };
  AssertFatal(dl_Bandwidth < 6, "dl_Bandwidth is 0..5\n");
  return (rbgmap[dl_Bandwidth]);
}


// This function assigns pre-available RBS to each UE in specified sub-bands before scheduling is done
void
dlsch_scheduler_pre_processor(module_id_t Mod_id,
                              int slice_idx,
                              frame_t frameP,
                              sub_frame_t subframeP,
                              int *mbsfn_flag,
                              uint8_t rballoc_sub[NFAPI_CC_MAX][N_RBG_MAX]) {
  int UE_id;
  uint8_t CC_id;
  uint16_t i, j;
  int min_rb_unit[NFAPI_CC_MAX];

  eNB_MAC_INST *eNB = RC.mac[Mod_id];
  slice_info_t *sli = &eNB->slice_info;
  uint16_t (*nb_rbs_required)[MAX_MOBILES_PER_ENB]  = sli->pre_processor_results[slice_idx].nb_rbs_required;
  uint16_t (*nb_rbs_accounted)[MAX_MOBILES_PER_ENB] = sli->pre_processor_results[slice_idx].nb_rbs_accounted;
  uint16_t (*nb_rbs_remaining)[MAX_MOBILES_PER_ENB] = sli->pre_processor_results[slice_idx].nb_rbs_remaining;
  uint8_t  (*MIMO_mode_indicator)[N_RBG_MAX]     = sli->pre_processor_results[slice_idx].MIMO_mode_indicator;

  UE_list_t *UE_list = &eNB->UE_list;
  UE_sched_ctrl *ue_sched_ctl;

  // Initialize scheduling information for all active UEs
  memset(&sli->pre_processor_results[slice_idx], 0, sizeof(sli->pre_processor_results[slice_idx]));
  // FIXME: After the memset above, some of the resets in reset() are redundant
  dlsch_scheduler_pre_processor_reset(Mod_id,
                                      slice_idx,
                                      frameP,
                                      subframeP,
                                      min_rb_unit,
                                      nb_rbs_required,
                                      rballoc_sub,
                                      MIMO_mode_indicator,
                                      mbsfn_flag); // FIXME: Not sure if useful
  // STATUS
  // Store the DLSCH buffer for each logical channel
  store_dlsch_buffer(Mod_id,
                     slice_idx,
                     frameP,
                     subframeP);

  // Calculate the number of RBs required by each UE on the basis of logical channel's buffer
  assign_rbs_required(Mod_id,
                      slice_idx,
                      frameP,
                      subframeP,
                      nb_rbs_required,
                      min_rb_unit);

  // Sorts the user on the basis of dlsch logical channel buffer and CQI
  sort_UEs(Mod_id,
           slice_idx,
           frameP,
           subframeP);

  // ACCOUNTING
  // This procedure decides the number of RBs to allocate
  dlsch_scheduler_pre_processor_accounting(Mod_id,
                                           slice_idx,
                                           frameP,
                                           subframeP,
                                           min_rb_unit,
                                           nb_rbs_required,
                                           nb_rbs_accounted);
  // POSITIONING
  // This procedure does the main allocation of the RBs
  dlsch_scheduler_pre_processor_positioning(Mod_id,
                                            slice_idx,
                                            min_rb_unit,
                                            nb_rbs_required,
                                            nb_rbs_accounted,
                                            nb_rbs_remaining,
                                            rballoc_sub,
                                            MIMO_mode_indicator);

  // SHARING
  // If there are available RBs left in the slice, allocate them to the highest priority UEs
  if (eNB->slice_info.intraslice_share_active) {
    dlsch_scheduler_pre_processor_intraslice_sharing(Mod_id,
                                                     slice_idx,
                                                     min_rb_unit,
                                                     nb_rbs_required,
                                                     nb_rbs_accounted,
                                                     nb_rbs_remaining,
                                                     rballoc_sub,
                                                     MIMO_mode_indicator);
  }

  for (UE_id = UE_list->head; UE_id >= 0; UE_id = UE_list->next[UE_id]) {
    ue_sched_ctl = &UE_list->UE_sched_ctrl[UE_id];

    for (i = 0; i < UE_num_active_CC(UE_list, UE_id); i++) {
      CC_id = UE_list->ordered_CCids[i][UE_id];
      //PHY_vars_eNB_g[Mod_id]->mu_mimo_mode[UE_id].dl_pow_off = dl_pow_off[UE_id];
      COMMON_channels_t *cc = &eNB->common_channels[CC_id];
      int N_RBG = to_rbg(cc->mib->message.dl_Bandwidth);

      if (ue_sched_ctl->pre_nb_available_rbs[CC_id] > 0) {
        LOG_D(MAC, "******************DL Scheduling Information for UE%d ************************\n",
              UE_id);
        LOG_D(MAC, "dl power offset UE%d = %d \n",
              UE_id,
              ue_sched_ctl->dl_pow_off[CC_id]);
        LOG_D(MAC, "***********RB Alloc for every subband for UE%d ***********\n",
              UE_id);

        for (j = 0; j < N_RBG; j++) {
          //PHY_vars_eNB_g[Mod_id]->mu_mimo_mode[UE_id].rballoc_sub[UE_id] = rballoc_sub_UE[CC_id][UE_id][UE_id];
          LOG_D(MAC, "RB Alloc for UE%d and Subband%d = %d\n",
                UE_id, j,
                ue_sched_ctl->rballoc_sub_UE[CC_id][j]);
        }

        //PHY_vars_eNB_g[Mod_id]->mu_mimo_mode[UE_id].pre_nb_available_rbs = pre_nb_available_rbs[CC_id][UE_id];
        LOG_D(MAC, "[eNB %d][SLICE %d]Total RBs allocated for UE%d = %d\n",
              Mod_id,
              eNB->slice_info.dl[slice_idx].id,
              UE_id,
              ue_sched_ctl->pre_nb_available_rbs[CC_id]);
      }
    }
  }
}