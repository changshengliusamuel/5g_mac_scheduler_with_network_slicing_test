#include "data_structure.h"
#include "intra_slice.h"
#include <pthread.h>

inter_slice(module_id, frame, subframe, slice_input[MAX_NUM_SLICE]){
	construct inter_slice_sched;
	construct UE_inform;
	if new_config_flag = 1{
		for i = 1 to MAX_NUM_SLICE{
			if (slice_input[i].pos_high > 0){
				inter_slice_sched.active_slice[i] = 1;
				// set the boundary of each slice position
				for j = 0 to  NFAPI_CC_MAX {
					 // calculate the maximum number of RB	
					inter_slice_sched.max_nb_rb[i] = (slice_input.pos_high - slice_input.pos_low +1) * RBG_unit; 
					inter_slice_sched.maxCCE[i] = get_nCCE_max();
					for k = 0 to N_RBG_MAX {
						if slice_input.pos_low < k;
						continue;
						inter_slice_sched.rballoc_slice[i].slice_alloc_mask[j][k]=1;
						if slice_input.pos_high >= k
						break;
					}
				}
			}
		} 
		new_config_flag = 0;
	} 		
    return inter_slice_sched;
}


if (UE_list.slice_id == intra_config.slice_id){
	intra_slice();
}


intra_slice(module_id, frame, subframe, intra_slice_input[slice_id]){
	construct intra_slice_sched_result;
	schedule_algorithum_t *schedule_algorithum;

	//caculate RLC buffer size
	for(UE_id = UE_list.head to UE_list.next)
	{
		for (lcid = 0 to MAX_NUM_LCID) 
		{
			rlc_status = mac_rlc_status_ind();
            UE_template->dl_buffer_info[lcid] = rlc_status.bytes_in_buffer;
            //storing the total dlsch buffer
            UE_inform[UE_id]->dl_buffer_total[slice_id] += UE_template->dl_buffer_info[lcid];
		}
	}

	// Update CQI information across component carriers
	for (int n = 0; n < UE_list->numactiveCCs[UE_id]; n++) {
      CC_id = UE_list->ordered_CCids[n][UE_id];
      eNB_UE_stats = &UE_list->eNB_UE_stats[CC_id][UE_id];
      UE_sched_result.mcs = cmin(cqi2mcs(intra_slice_input.cqi[CC_id]),intra_slice_input.intra_config.maxmcs);
    }
    // caculate the number of RBs required according to buffer size. assign_rbs_required() 
	for (UE_id = UE_list.head to UE_list.next) {
		if (UE_inform->dl_buffer_total > 0){
			nb_rbs_required ; // caculate the number of RBs required store in nb_rbs_required
			TBS =  get_TBS_DL(UE_sched_result.mcs, intra_slice_input.inter_slice_sched.max_nb_rb[slice_id]);
		}
		while(TBS<UE_inform->dl_buffer_total){
			nb_rbs_required += RBG_unit ; // keep caculate the number of RBs required store in nb_rbs_required
			TBS =  get_TBS_DL(UE_sched_result.mcs,nb_rbs_required);
		}
	}

	for (int j = 0; j < NFAPI_CC_MAX; j++)
	{
		remaining_CCE[slice_id][j] = intra_slice_input[slice_id]->maxCCE[slice_id][j] - intra_slice_input[slice_id]->maxCCE_UL[slice_id][j];
	}

	for (int k = 0; i < count; ++k)
	{
		/* code */
	}
	remaining_CCE = caculate_num_CCE(intra_slice_input[slice_id]->maxCCE);
	if(remaining_CCE > 0){
		sort_UE(); 	//  Sorts the user according to UE_list->sorting_criteria
		int (*schedule_algorithum)(int);
		schedule_algorithum = round_robin;
	}

	confirm if there are any remaining RBs in this slice that are still empty.
	store the result in intra_slice_sched_result.nb_rbs_remaining_slice;
	return intra_slice_sched_result;
}


final_scheduler(intra_slice_result, dl_config_request_body){
	for UE_id = 0 to N do
		if (retransmission)
			start to fill nfapi dlsch config;
			prgram dlsch nack;
		else 
			add the length for  all the control elements(TA, drx, etc) : header + payload;
			generate dlsch header;
			start to fill nfapi dlsch config;
			prgram dlsch ack;
		end if;
	end for;
	// start to fill DLSCH DCI;
	fill_DLSCH_dci();
	return nfapi_dl_config_request_pdu_t;
}

round_robin()
{



	// FIXME: This is not ideal, why loop on UEs to find average_rbs_per_user[], that is per-CC?
      // TODO: Look how to loop on active CCs only without using the UE_num_active_CC() function.
      for (UE_id = UE_list->head; UE_id >= 0; UE_id = UE_list->next[UE_id]) {
        rnti = UE_RNTI(Mod_id, UE_id);

        if (rnti == NOT_A_RNTI) continue;

        if (UE_list->UE_sched_ctrl[UE_id].ul_out_of_sync == 1) continue;

        if (!ue_dl_slice_membership(Mod_id, UE_id, slice_idx)) continue;

        for (i = 0; i < UE_num_active_CC(UE_list, UE_id); ++i) {
          CC_id = UE_list->ordered_CCids[i][UE_id];
          ue_sched_ctl = &UE_list->UE_sched_ctrl[UE_id];
          available_rbs[CC_id] = ue_sched_ctl->max_rbs_allowed_slice[CC_id][slice_idx];

          if (ue_count_newtx[CC_id] == 0) {
            average_rbs_per_user[CC_id] = 0;
          } else if (min_rb_unit[CC_id]*ue_count_newtx[CC_id] <= available_rbs[CC_id]) {
            average_rbs_per_user[CC_id] = (uint16_t)floor(available_rbs[CC_id]/ue_count_newtx[CC_id]);
          } else {
            // consider the total number of use that can be scheduled UE
            average_rbs_per_user[CC_id] = (uint16_t)min_rb_unit[CC_id];
          }
        }
      }

      // note: nb_rbs_required is assigned according to total_buffer_dl
      // extend nb_rbs_required to capture per LCID RB required
      for (UE_id = UE_list->head; UE_id >= 0; UE_id = UE_list->next[UE_id]) {
        rnti = UE_RNTI(Mod_id, UE_id);

        if (rnti == NOT_A_RNTI) continue;

        if (UE_list->UE_sched_ctrl[UE_id].ul_out_of_sync == 1) continue;

        if (!ue_dl_slice_membership(Mod_id, UE_id, slice_idx)) continue;

        for (i = 0; i < UE_num_active_CC(UE_list, UE_id); i++) {
          CC_id = UE_list->ordered_CCids[i][UE_id];
          nb_rbs_accounted[CC_id][UE_id] = cmin(average_rbs_per_user[CC_id], nb_rbs_required[CC_id][UE_id]);
        }
      }
}