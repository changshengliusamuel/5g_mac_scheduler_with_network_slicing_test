#include "data_structure.h"
#include "intra_slice.h"
#include <pthread.h>

inter_slice(module_id_t module_id, frame_t frame, sub_frame_t subframe, slice_input_t *slice_input /*slice_input[MAX_NUM_SLICE]*/)
{
	inter_slice_sched_t inter_slice_sched;
	slice_input_t *slice_input_temp;
	slice_input_temp= (slice_input_t*)molloc(sizeof(slice_input_t)*MAX_NUM_SLICE);
	eNB_MAC_INST *eNB = RC.mac[module_idP];
	UE_list_t *UE_list= &eNB->UE_list;
	/*
	compare current configuration and new configuration
	if(differnt)
	{
		new_config_flag=1;
	}else
	{
		new_config_flag=0;
	}
	*/
	int new_config_flag=0;
	/*Confirm whether the slice configuration of this schedule has changed or not.
	If there is new configuration, set new_config_flag=1*/
	for (int slice_index = 0; slice_index < MAX_NUM_SLICE; slice_index++)
	{
		if(slice_input_temp[slice_index].slice_id!=slice_input[slice_index].slice_id)
		{
			new_config_flag=1;
			break;
		}
		for (int cc_id_index = 0; cc_id_index < NFAPI_CC_MAX; ++cc_id_index)
		{
			if (slice_input_temp[slice_index].pos_high[cc_id_index]!=slice_input[slice_index].pos_high[cc_id_index]||
				slice_input_temp[slice_index].pos_low[cc_id_index]!=slice_input[slice_index].pos_low[cc_id_index]||
				slice_input_temp[slice_index].pct[cc_id_index]!=slice_input[slice_index].pct[cc_id_index]
				)
			{
				new_config_flag=1;
				break;
			}
		}
		if (new_config_flag==1)
		{
			break;
		}			
	}
	/*Calculate the boundary and the maximum number of available RBs and CCEs for the slice with the new configuration.*/
	if (new_config_flag == 1
	{
		for (i = 0 ;i< MAX_NUM_SLICE;i++)
		{
			for (j = 0 ;j<NFAPI_CC_MAX;j++)
			{
				if(slice_input[i].pos_high[j] > 0)
				{
					inter_slice_sched.active_slice[i] = 1;
					// calculate the maximum number of RB 
					inter_slice_sched.max_nb_rb[i][j] = (slice_input.pos_high[j] - slice_input.pos_low[j] +1) * RBG_unit;
					// calculate the maximum number of CCE
					inter_slice_sched.maxCCE[i][j]= get_nCCE_max();
					// set the boundary of each slice position
					for (k = 0 ;k<N_RBG_MAX;k++) 
					{
						if (slice_input[i].pos_low[j] < k)
						{
							continue;	
						}
						inter_slice_sched.rballoc_slice[i].slice_alloc_mask[j][k]=1;
						if (slice_input[i].pos_high[j] >= k)
						{
							break;
						}
					}
				}
			}
		} 
		new_config_flag = 0;
	} 
  	//caculate RLC buffer size
  	for (int current_slice_index = 0; current_slice_index < MAX_NUM_SLICES; ++current_slice_index)
  	{
  		if (inter_slice_sched.active_slice[current_slice_index] == 1 )
  		{
			for (int UE_id = 0 ;UE_id<MAX_MOBILES_PER_ENB;UE_id++)
			{
				for (int lcid = 0 ;lcid<MAX_NUM_LCID ;lcid++)
				{
					mac_rlc_status_resp_t rlc_status = mac_rlc_status_ind(module_idP,
  																		  rntiP,
																		  eNB_index,
																		  frameP,
																		  subframeP,
																		  enb_flagP,
																		  MBMS_flagP,
																		  lcid,
																		  tb_sizeP);
	                UE_list->UE_template->dl_buffer_info[lcid] = rlc_status.bytes_in_buffer;
	                //storing the total dlsch buffer for each active slice
	                UE_list->UE_inform[UE_id]->dl_buffer_total[current_slice_index] += UE_template->dl_buffer_info[lcid];
				}
			}         
		}	
  	}
	
    return inter_slice_sched;
}
