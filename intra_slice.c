void schedule_SI_in_intra_slice(module_id_t module_idP, frame_t frameP, sub_frame_t subframeP)
{

  int8_t bcch_sdu_length;
  int mcs = -1;
  int CC_id;
  eNB_MAC_INST *eNB = RC.mac[module_idP];
  COMMON_channels_t *cc;
  uint8_t *vrb_map;
  int first_rb = -1;
  int N_RB_DL;
  nfapi_dl_config_request_t      *dl_config_request;
  nfapi_dl_config_request_pdu_t  *dl_config_pdu;
  nfapi_tx_request_pdu_t *TX_req;
  nfapi_dl_config_request_body_t *dl_req;
  uint16_t sfn_sf = frameP << 4 | subframeP;

  start_meas(&eNB->schedule_si);

    // Only schedule LTE System Information in subframe 5
  if (subframeP == 5) {

    for (CC_id = 0; CC_id < MAX_NUM_CCs; CC_id++) {

      cc = &eNB->common_channels[CC_id];
      vrb_map = (void *) &cc->vrb_map;
      N_RB_DL = to_prb(cc->mib->message.dl_Bandwidth);
      dl_config_request = &eNB->DL_req[CC_id];
      dl_req = &eNB->DL_req[CC_id].dl_config_request_body;


      bcch_sdu_length = mac_rrc_data_req(module_idP, CC_id, frameP, BCCH, 0xFFFF,1, &cc->BCCH_pdu.payload[0], 0); // not used in this case

      if (bcch_sdu_length > 0) {
        LOG_D(MAC, "[eNB %d] Frame %d : BCCH->DLSCH CC_id %d, Received %d bytes \n", module_idP, frameP, CC_id, bcch_sdu_length);

    // Allocate 4 PRBs in a random location
    /*
       while (1) {
       first_rb = (unsigned char)(taus()%(PHY_vars_eNB_g[module_idP][CC_id]->frame_parms.N_RB_DL-4));
       if ((vrb_map[first_rb] != 1) && 
       (vrb_map[first_rb+1] != 1) && 
       (vrb_map[first_rb+2] != 1) && 
       (vrb_map[first_rb+3] != 1))
       break;
       }
     */
        switch (N_RB_DL) {
          case 6:
          first_rb = 0;
          break;
          case 15:
          first_rb = 6;
          break;
          case 25:
          first_rb = 11;
          break;
          case 50:
          first_rb = 23;
          break;
          case 100:
          first_rb = 48;
          break;
        }

        vrb_map[first_rb] = 1;
        vrb_map[first_rb + 1] = 1;
        vrb_map[first_rb + 2] = 1;
        vrb_map[first_rb + 3] = 1;

    // Get MCS for length of SI, 3 PRBs
        if (bcch_sdu_length <= 7) {
          mcs = 0;
        } else if (bcch_sdu_length <= 11) {
          mcs = 1;
        } else if (bcch_sdu_length <= 18) {
          mcs = 2;
        } else if (bcch_sdu_length <= 22) {
          mcs = 3;
        } else if (bcch_sdu_length <= 26) {
          mcs = 4;
        } else if (bcch_sdu_length <= 28) {
          mcs = 5;
        } else if (bcch_sdu_length <= 32) {
          mcs = 6;
        } else if (bcch_sdu_length <= 41) {
          mcs = 7;
        } else if (bcch_sdu_length <= 49) {
          mcs = 8;
        } else if (bcch_sdu_length <= 59) {
          mcs = 9;
        } 
        else AssertFatal(1==0,"Cannot Assign mcs for bcch_sdu_length %d (max mcs 9)\n",bcch_sdu_length);
      }
    }
  }
}
void schedule_PCH_in_intra_slice(module_id_t module_idP,frame_t frameP,sub_frame_t subframeP) 
{
  /* DCI:format 1A/1C P-RNTI:0xFFFE */
  /* PDU:eNB_rrc_inst[Mod_idP].common_channels[CC_id].PCCH_pdu.payload */
  uint16_t pcch_sdu_length;
  int mcs = -1;
  int CC_id;
  eNB_MAC_INST *eNB = RC.mac[module_idP];
  COMMON_channels_t *cc;
  uint8_t *vrb_map;
  int n_rb_dl;
  int first_rb = -1;
  nfapi_dl_config_request_pdu_t *dl_config_pdu;
  nfapi_tx_request_pdu_t *TX_req;
  nfapi_dl_config_request_body_t *dl_req;
  UE_PF_PO_t *ue_pf_po;
#ifdef FORMAT1C
  int gap_index = 0;      /* indicate which gap(1st or 2nd) is used (0:1st) */
  const int GAP_MAP [9][2] = {
    {-1, 0},        /* N_RB_DL [6-10] -1: |N_RB/2| 0: N/A*/
    {4, 0},         /* N_RB_DL [11] */
    {8, 0},         /* N_RB_DL [12-19] */
    {12, 0},        /* N_RB_DL [20-26] */
    {18, 0},        /* N_RB_DL [27-44] */
    {27, 0},        /* N_RB_DL [45-49] */
    {27, 9},        /* N_RB_DL [50-63] */
    {32, 16},       /* N_RB_DL [64-79] */
    {48, 16}        /* N_RB_DL [80-110] */
  };
  uint8_t n_rb_step = 0;
  uint8_t n_gap = 0;
  uint8_t n_vrb_dl = 0;
  uint8_t Lcrbs = 0;
  uint16_t rb_bit = 168;    /* RB bit number value is unsure */
#endif
  start_meas(&eNB->schedule_pch);

  for (CC_id = 0; CC_id < RC.nb_mac_CC[module_idP]; CC_id++) {
    cc              = &eNB->common_channels[CC_id];
    vrb_map         = (void *) &cc->vrb_map;
    n_rb_dl         = to_prb(cc->mib->message.dl_Bandwidth);
    dl_req          = &eNB->DL_req[CC_id].dl_config_request_body;

    for (uint16_t i = 0; i < MAX_MOBILES_PER_ENB; i++) {
      ue_pf_po = &UE_PF_PO[CC_id][i];

      if (ue_pf_po->enable_flag != TRUE) {
        continue;
      }

      if (frameP % ue_pf_po->T == ue_pf_po->PF_min && subframeP == ue_pf_po->PO) {
        pcch_sdu_length = mac_rrc_data_req(module_idP,
         CC_id,
         frameP,
         PCCH,
         0xFFFE,
         1,
         &cc->PCCH_pdu.payload[0],
                                           i); // used for ue index

        if (pcch_sdu_length == 0) {
          LOG_D(MAC, "[eNB %d] Frame %d subframe %d: PCCH not active(size = 0 byte)\n",
            module_idP,
            frameP,
            subframeP);
          continue;
        }

        LOG_D(MAC, "[eNB %d] Frame %d subframe %d: PCCH->PCH CC_id %d UE_id %d, Received %d bytes \n",
          module_idP,
          frameP,
          subframeP,
          CC_id,
          i,
          pcch_sdu_length);
#ifdef FORMAT1C

        //NO SIB
        if ((subframeP == 0 || subframeP == 1 || subframeP == 2 || subframeP == 4 || subframeP == 6 || subframeP == 9) ||
          (subframeP == 5 && ((frameP % 2) != 0 && (frameP % 8) != 1))) {
          switch (n_rb_dl) {
            case 25:
              n_gap = GAP_MAP[3][0];  /* expect: 12 */
              n_vrb_dl = 2*((n_gap < (n_rb_dl - n_gap)) ? n_gap : (n_rb_dl - n_gap));  /* expect: 24 */
            first_rb = 10;
            break;

            case 50:
              n_gap = GAP_MAP[6][gap_index];  /* expect: 27 or 9 */

            if (gap_index > 0) {
                n_vrb_dl = (n_rb_dl / (2*n_gap)) * (2*n_gap);  /* 36 */
            } else {
                n_vrb_dl = 2*((n_gap < (n_rb_dl - n_gap)) ? n_gap : (n_rb_dl - n_gap));  /* expect: 46 */
            }

            first_rb = 24;
            break;

            case 100:
              n_gap = GAP_MAP[8][gap_index];  /* expect: 48 or 16 */

            if (gap_index > 0) {
                n_vrb_dl = (n_rb_dl / (2*n_gap)) * (2*n_gap);  /* expect: 96 */
            } else {
                n_vrb_dl = 2*((n_gap < (n_rb_dl - n_gap)) ? n_gap : (n_rb_dl - n_gap));  /* expect: 96 */
            }

            first_rb = 48;
            break;
          }
        } else if (subframeP == 5 && ((frameP % 2) == 0 || (frameP % 8) == 1)) {  // SIB + paging
          switch (n_rb_dl) {
            case 25:
              n_gap = GAP_MAP[3][0];  /* expect: 12 */
              n_vrb_dl = 2*((n_gap < (n_rb_dl - n_gap)) ? n_gap : (n_rb_dl - n_gap));  /* expect: 24 */
            first_rb = 14;
            break;

            case 50:
              n_gap = GAP_MAP[6][gap_index];  /* expect: 27 or 9 */

            if (gap_index > 0) {
                n_vrb_dl = (n_rb_dl / (2*n_gap)) * (2*n_gap);  /* 36 */
            } else {
                n_vrb_dl = 2*((n_gap < (n_rb_dl - n_gap)) ? n_gap : (n_rb_dl - n_gap));  /* expect: 46 */
            }

            first_rb = 28;
            break;

            case 100:
              n_gap = GAP_MAP[8][gap_index];  /* expect: 48 or 16 */

            if (gap_index > 0) {
                n_vrb_dl = (n_rb_dl / (2*n_gap)) * (2*n_gap);  /* expect: 96 */
            } else {
                n_vrb_dl = 2*((n_gap < (n_rb_dl - n_gap)) ? n_gap : (n_rb_dl - n_gap));  /* expect: 96 */
            }

            first_rb = 52;
            break;
          }
        }

        /* Get MCS for length of PCH */
        if (pcch_sdu_length <= TBStable1C[0]) {
          mcs=0;
        } else if (pcch_sdu_length <= TBStable1C[1]) {
          mcs=1;
        } else if (pcch_sdu_length <= TBStable1C[2]) {
          mcs=2;
        } else if (pcch_sdu_length <= TBStable1C[3]) {
          mcs=3;
        } else if (pcch_sdu_length <= TBStable1C[4]) {
          mcs=4;
        } else if (pcch_sdu_length <= TBStable1C[5]) {
          mcs=5;
        } else if (pcch_sdu_length <= TBStable1C[6]) {
          mcs=6;
        } else if (pcch_sdu_length <= TBStable1C[7]) {
          mcs=7;
        } else if (pcch_sdu_length <= TBStable1C[8]) {
          mcs=8;
        } else if (pcch_sdu_length <= TBStable1C[9]) {
          mcs=9;
        } else {
          /* unexpected: pcch sdb size is over max value*/
          LOG_E(MAC,"[eNB %d] Frame %d : PCCH->PCH CC_id %d, Received %d bytes is over max length(256) \n",
            module_idP,
            frameP,
            CC_id,
            pcch_sdu_length);
          return;
        }

        rb_num = TBStable1C[mcs] / rb_bit + ( (TBStable1C[mcs] % rb_bit == 0)? 0: 1) + 1;

        /* calculate N_RB_STEP and Lcrbs */
        if (n_rb_dl < 50) {
          n_rb_step = 2;
          Lcrbs = rb_num / 2 + ((rb_num % 2 == 0) ? 0:2);
        } else {
          n_rb_step = 4;
          Lcrbs = rb_num / 4 + ((rb_num % 4 == 0) ? 0:4);
        }

        for(i = 0; i < Lcrbs ; i++) {
          vrb_map[first_rb+i] = 1;
        }

#else

        //NO SIB
        if ((subframeP == 0 || subframeP == 1 || subframeP == 2 || subframeP == 4 || subframeP == 6 || subframeP == 9) ||
          (subframeP == 5 && ((frameP % 2) != 0 && (frameP % 8) != 1))) {
          switch (n_rb_dl) {
            case 25:
            first_rb = 10;
            break;

            case 50:
            first_rb = 24;
            break;

            case 100:
            first_rb = 48;
            break;
          }
        } else if (subframeP == 5 && ((frameP % 2) == 0 || (frameP % 8) == 1)) {  // SIB + paging
          switch (n_rb_dl) {
            case 25:
            first_rb = 14;
            break;

            case 50:
            first_rb = 28;
            break;

            case 100:
            first_rb = 52;
            break;
          }
        }

        vrb_map[first_rb] = 1;
        vrb_map[first_rb + 1] = 1;
        vrb_map[first_rb + 2] = 1;
        vrb_map[first_rb + 3] = 1;

        /* Get MCS for length of PCH */
        if (pcch_sdu_length <= get_TBS_DL(0, 3)) {
          mcs = 0;
        } else if (pcch_sdu_length <= get_TBS_DL(1, 3)) {
          mcs = 1;
        } else if (pcch_sdu_length <= get_TBS_DL(2, 3)) {
          mcs = 2;
        } else if (pcch_sdu_length <= get_TBS_DL(3, 3)) {
          mcs = 3;
        } else if (pcch_sdu_length <= get_TBS_DL(4, 3)) {
          mcs = 4;
        } else if (pcch_sdu_length <= get_TBS_DL(5, 3)) {
          mcs = 5;
        } else if (pcch_sdu_length <= get_TBS_DL(6, 3)) {
          mcs = 6;
        } else if (pcch_sdu_length <= get_TBS_DL(7, 3)) {
          mcs = 7;
        } else if (pcch_sdu_length <= get_TBS_DL(8, 3)) {
          mcs = 8;
        } else if (pcch_sdu_length <= get_TBS_DL(9, 3)) {
          mcs = 9;
        }

#endif
      }
    }
  }
}
void schedule_RA_in_intra_slice(module_id_t module_idP, frame_t frameP, sub_frame_t subframeP) {
  int CC_id;
  eNB_MAC_INST *mac = RC.mac[module_idP];
  COMMON_channels_t *cc = mac->common_channels;
  RA_t *ra;
  uint8_t i;
  start_meas(&mac->schedule_ra);

  for (CC_id = 0; CC_id < MAX_NUM_CCs; CC_id++) {
    // skip UL component carriers if TDD
    if (is_UL_sf(&cc[CC_id], subframeP) == 1)
      continue;

    for (i = 0; i < NB_RA_PROC_MAX; i++) {
      ra = (RA_t *) & cc[CC_id].ra[i];

      //LOG_D(MAC,"RA[state:%d]\n",ra->state);

      if (ra->state == MSG2)
        generate_Msg2_in_intra_slice(module_idP, CC_id, frameP, subframeP, ra);
      else if (ra->state == MSG4 && ra->Msg4_frame == frameP && ra->Msg4_subframe == subframeP )
        generate_Msg4(module_idP, CC_id, frameP, subframeP, ra);
      else if (ra->state == WAITMSG4ACK)
        check_Msg4_retransmission(module_idP, CC_id, frameP,
          subframeP, ra);
    }     // for i=0 .. N_RA_PROC-1
  }       // CC_id

  stop_meas(&mac->schedule_ra);
}
void generate_Msg2_in_intra_slice(module_id_t module_idP,
 int CC_idP,
 frame_t frameP,
 sub_frame_t subframeP,
 RA_t *ra)
//------------------------------------------------------------------------------
{
  eNB_MAC_INST *mac = RC.mac[module_idP];
  COMMON_channels_t *cc = mac->common_channels;
  uint8_t *vrb_map = NULL;
  int first_rb = 0;
  int N_RB_DL = 0;
  nfapi_dl_config_request_pdu_t *dl_config_pdu = NULL;
  nfapi_tx_request_pdu_t *TX_req = NULL;
  nfapi_dl_config_request_body_t *dl_req_body = NULL;
  vrb_map = cc[CC_idP].vrb_map;
  dl_req_body = &mac->DL_req[CC_idP].dl_config_request_body;
  dl_config_pdu = &dl_req_body->dl_config_pdu_list[dl_req_body->number_pdu];
  N_RB_DL = to_prb(cc[CC_idP].mib->message.dl_Bandwidth);

  if ((ra->Msg2_frame == frameP) && (ra->Msg2_subframe == subframeP)) {
    LOG_D(MAC,
      "[eNB %d] CC_id %d Frame %d, subframeP %d: Generating RAR DCI, state %d\n",
      module_idP, CC_idP, frameP, subframeP, ra->state);
      // Allocate 4 PRBS starting in RB 0
    first_rb = 0;
    vrb_map[first_rb] = 1;
    vrb_map[first_rb + 1] = 1;
    vrb_map[first_rb + 2] = 1;
    vrb_map[first_rb + 3] = 1;
  }
}
void schedule_ulsch_rnti(module_id_t   module_idP,
  int           slice_idx,
  frame_t       frameP,
  sub_frame_t   subframeP,
  unsigned char sched_subframeP,
  uint16_t      *first_rb)
//-----------------------------------------------------------------------------
{
  rnti_t rnti = -1;
  uint8_t aggregation = 2;
  uint8_t round_index = 0;
  uint8_t harq_pid = 0;
  uint8_t status = 0;
  uint8_t rb_table_index = -1;
  uint8_t dlsch_flag = 0;
  uint16_t ul_req_index = 0;
  uint32_t cqi_req = 0;
  uint32_t cshift = 0;
  uint32_t ndi = 0;
  uint32_t tpc = 0;
  int32_t normalized_rx_power = 0;
  int32_t target_rx_power = 0;
  int32_t framex10psubframe = 0;
  static int32_t tpc_accumulated = 0;
  int sched_frame = 0;
  int CC_id = 0;
  eNB_MAC_INST *mac = NULL;
  COMMON_channels_t *cc = NULL;
  UE_list_t *UE_list = NULL;
  slice_info_t *sli = NULL;
  UE_TEMPLATE *UE_template_ptr = NULL;
  UE_sched_ctrl *UE_sched_ctrl_ptr = NULL;
  int rvidx_tab[4] = {0, 2, 3, 1};
  int first_rb_slice[NFAPI_CC_MAX];
  int n_rb_ul_tab[NFAPI_CC_MAX];
  /* Init */
  mac = RC.mac[module_idP];
  cc = mac->common_channels;
  UE_list = &(mac->UE_list);
  sli = &(mac->slice_info);
  memset(first_rb_slice, 0, NFAPI_CC_MAX * sizeof(int));
  memset(n_rb_ul_tab, 0, NFAPI_CC_MAX * sizeof(int));
  sched_frame = frameP;

  if (sched_subframeP < subframeP) {
    sched_frame++;
    sched_frame %= 1024;
  }

  /* NFAPI struct init */
  nfapi_hi_dci0_request_t        *hi_dci0_req      = &(mac->HI_DCI0_req[CC_id][subframeP]);
  nfapi_hi_dci0_request_body_t   *hi_dci0_req_body = &(hi_dci0_req->hi_dci0_request_body);
  nfapi_hi_dci0_request_pdu_t    *hi_dci0_pdu;
  nfapi_ul_config_request_t      *ul_req_tmp       = &(mac->UL_req_tmp[CC_id][sched_subframeP]);
  nfapi_ul_config_request_body_t *ul_req_tmp_body  = &(ul_req_tmp->ul_config_request_body);
  nfapi_ul_config_ulsch_harq_information *ulsch_harq_information;
  hi_dci0_req->sfn_sf = (frameP << 4) + subframeP;

  /* Note: RC.nb_mac_CC[module_idP] should be lower than or equal to NFAPI_CC_MAX */
  for (CC_id = 0; CC_id < RC.nb_mac_CC[module_idP]; CC_id++) {
    n_rb_ul_tab[CC_id] = to_prb(cc[CC_id].ul_Bandwidth); // return total number of PRB
    /* HACK: let's remove the PUCCH from available RBs
     * we suppose PUCCH size is:
     * - for 25 RBs: 1 RB (top and bottom of ressource grid)
     * - for 50:     2 RBs
     * - for 100:    3 RBs
     * This is totally arbitrary and might even be wrong.
     * We suppose 'first_rb[]' has been correctly populated by the caller,
     * so we only remove the top part of the resource grid.
     */
    switch (n_rb_ul_tab[CC_id]) {
      case 25:  n_rb_ul_tab[CC_id] -= 1; break;
      case 50:  n_rb_ul_tab[CC_id] -= 2; break;
      case 100: n_rb_ul_tab[CC_id] -= 3; break;
      default: LOG_E(MAC, "RBs setting not handled. Todo.\n"); exit(1);
    }
    UE_list->first_rb_offset[CC_id][slice_idx] = cmin(n_rb_ul_tab[CC_id], sli->ul[slice_idx].first_rb);
  }

  /* 
   * ULSCH preprocessor: set UE_template->
   * pre_allocated_nb_rb_ul[slice_idx]
   * pre_assigned_mcs_ul
   * pre_allocated_rb_table_index_ul
   */
  ulsch_scheduler_pre_processor(module_idP, slice_idx, frameP, subframeP, sched_frame, sched_subframeP, first_rb);

  for (CC_id = 0; CC_id < RC.nb_mac_CC[module_idP]; CC_id++) {
    first_rb_slice[CC_id] = first_rb[CC_id] + UE_list->first_rb_offset[CC_id][slice_idx];
  }
}
